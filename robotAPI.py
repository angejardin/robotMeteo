from contextlib import closing
from urllib.request import urlopen
import json
import datetime
import requests
import os

urlmongo = os.environ['URL_MONGO'] 
tokenmeteo = os.environ['TOKEN_METEO'] 
urlmeteo = os.environ['URL_METEO'] 
url1 = os.environ['URL_SIGNIN'] 
url2 = os.environ['URL_INSEE'] 
idrobot = os.environ['ID_ROBOT'] 
mdprobot = os.environ['MDP_ROBOT'] 
        

#Connexion à la base de données
def get_db():
    from pymongo import MongoClient
    client = MongoClient(urlmongo)
    db = client.testJardin
    return db


#Insertion de donnée dans la base de données
def insertData(data):
    db = get_db() 
    db.meteo.insert_one(data)


#Suppression de donnée dans la base de données
def deleteData(data):
    db = get_db() 
    db.meteo.delete_many({"datetime" : {"$regex" : ".*" + data + ".*"}})


#Récupération des information météorologiques et insertion dans la base de données
def recupInfos():    
    codesinsee = recupInsee()
    #Pour tous les codes insee des utilisateurs de l'application Ange jardin
    for insee in codesinsee:         
        if insee != "": #Suppression du code insee vide correspondant aux informations vides du robot        
            #### Pour obtenir la météo du jour du code insee
            with closing(urlopen(urlmeteo + tokenmeteo + "&insee=" + insee)) as f: 
                decoded = json.loads(f.read())
                (city,forecast) = (decoded[k] for k in ('city','forecast'))

            forecast["insee"] = insee
            insertData(forecast) 


#Suppression de la météo de plus de 16 jours    
def deleteInfos():    
    myDate = datetime.date.today(); td = datetime.timedelta(days = 16); 
    myquery = str(myDate - td)
    deleteData(myquery)


#Récupération de la liste des codes insee des utilisateurs de l'application Ange jardin
def recupInsee():
    #Récupération du token du robot    
    body = {
        "username": idrobot,
        "password": mdprobot
    }
    headers1 = {'content-type': 'application/json'}
    repsignin = requests.post(url1, data=json.dumps(body), headers=headers1)
    token = json.loads(repsignin.content)['accessToken']

    #Récupération de la liste des codes insee    
    headers2 = {'Authorization': 'Bearer ' + token}
    repinsee = requests.get(url2, headers=headers2)

    return json.loads(repinsee.content)


#Fonction principale
if __name__ == "__main__":
    deleteInfos()
    recupInfos()                                                   
